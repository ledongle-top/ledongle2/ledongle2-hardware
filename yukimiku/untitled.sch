<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.4.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="ESP-WROOM-32">
<packages>
<package name="MODULE_ESP-WROOM-32">
<wire x1="-9" y1="-9.755" x2="9" y2="-9.755" width="0.127" layer="51"/>
<wire x1="9" y1="-9.755" x2="9" y2="15.745" width="0.127" layer="51"/>
<wire x1="9" y1="15.745" x2="-9" y2="15.745" width="0.127" layer="51"/>
<wire x1="-9" y1="15.745" x2="-9" y2="-9.755" width="0.127" layer="51"/>
<wire x1="-9" y1="-9" x2="-9" y2="-9.75" width="0.127" layer="21"/>
<wire x1="-9" y1="-9.75" x2="-6.5" y2="-9.75" width="0.127" layer="21"/>
<wire x1="6.5" y1="-9.75" x2="9" y2="-9.75" width="0.127" layer="21"/>
<wire x1="9" y1="-9.75" x2="9" y2="-9" width="0.127" layer="21"/>
<wire x1="-9" y1="9.25" x2="-9" y2="15.75" width="0.127" layer="21"/>
<wire x1="-9" y1="15.75" x2="9" y2="15.75" width="0.127" layer="21"/>
<wire x1="9" y1="15.75" x2="9" y2="9.25" width="0.127" layer="21"/>
<rectangle x1="-8.002640625" y1="10.5035" x2="-7.5" y2="15" layer="21"/>
<rectangle x1="-8.00838125" y1="14.5152" x2="-5" y2="15" layer="21"/>
<rectangle x1="-5.51458125" y1="12.0318" x2="-5" y2="14.5" layer="21"/>
<rectangle x1="-5.50796875" y1="12.0174" x2="-2.5" y2="12.5" layer="21"/>
<rectangle x1="-3.00556875" y1="12.0222" x2="-2.5" y2="14.5" layer="21"/>
<rectangle x1="-3.00538125" y1="14.526" x2="0" y2="15" layer="21"/>
<rectangle x1="-0.5019" y1="12.0456" x2="0" y2="14.5" layer="21"/>
<rectangle x1="-0.501415625" y1="12.0339" x2="2.5" y2="12.5" layer="21"/>
<rectangle x1="2.00635" y1="12.0381" x2="2.5" y2="14.5" layer="21"/>
<rectangle x1="2.00956875" y1="14.4994" x2="7.5" y2="15" layer="21"/>
<rectangle x1="7.01686875" y1="10.0241" x2="7.5" y2="15" layer="21"/>
<rectangle x1="4.51203125" y1="10.0268" x2="5" y2="15" layer="21"/>
<rectangle x1="7.00575" y1="9.507809375" x2="7.5" y2="10" layer="21"/>
<rectangle x1="4.5065" y1="9.51371875" x2="5" y2="10" layer="21"/>
<rectangle x1="-8.023740625" y1="10.0296" x2="-7.5" y2="10.5" layer="21"/>
<rectangle x1="2.00168125" y1="13.5114" x2="2.5" y2="15" layer="21"/>
<rectangle x1="-8.024790625" y1="10.5325" x2="-7.5" y2="15" layer="51"/>
<rectangle x1="-8.0219" y1="14.5397" x2="-5" y2="15" layer="51"/>
<rectangle x1="-5.51781875" y1="12.0389" x2="-5" y2="14.5" layer="51"/>
<rectangle x1="-5.514790625" y1="12.0323" x2="-2.5" y2="12.5" layer="51"/>
<rectangle x1="-3.003990625" y1="12.016" x2="-2.5" y2="14.5" layer="51"/>
<rectangle x1="-3.00396875" y1="14.5192" x2="0" y2="15" layer="51"/>
<rectangle x1="-0.500765625" y1="12.0184" x2="0" y2="14.5" layer="51"/>
<rectangle x1="-0.50015625" y1="12.0037" x2="2.5" y2="12.5" layer="51"/>
<rectangle x1="2.005740625" y1="12.0344" x2="2.5" y2="14.5" layer="51"/>
<rectangle x1="2.010809375" y1="14.5084" x2="7.5" y2="15" layer="51"/>
<rectangle x1="7.01163125" y1="10.0166" x2="7.5" y2="15" layer="51"/>
<rectangle x1="4.5081" y1="10.018" x2="5" y2="15" layer="51"/>
<rectangle x1="7.0176" y1="9.523890625" x2="7.5" y2="10" layer="51"/>
<rectangle x1="4.513140625" y1="9.527740625" x2="5" y2="10" layer="51"/>
<rectangle x1="-8.01843125" y1="10.023" x2="-7.5" y2="10.5" layer="51"/>
<rectangle x1="2.00256875" y1="13.5173" x2="2.5" y2="15" layer="51"/>
<wire x1="-9.25" y1="16" x2="9.25" y2="16" width="0.05" layer="39"/>
<wire x1="9.25" y1="16" x2="9.25" y2="9" width="0.05" layer="39"/>
<wire x1="9.25" y1="9" x2="10.1" y2="9" width="0.05" layer="39"/>
<wire x1="10.1" y1="9" x2="10.1" y2="-9" width="0.05" layer="39"/>
<wire x1="10.1" y1="-9" x2="9.25" y2="-9" width="0.05" layer="39"/>
<wire x1="9.25" y1="-9" x2="9.25" y2="-10" width="0.05" layer="39"/>
<wire x1="9.25" y1="-10" x2="6.5" y2="-10" width="0.05" layer="39"/>
<wire x1="6.5" y1="-10" x2="6.5" y2="-10.855" width="0.05" layer="39"/>
<wire x1="6.5" y1="-10.855" x2="-6.5" y2="-10.855" width="0.05" layer="39"/>
<wire x1="-6.5" y1="-10.855" x2="-6.5" y2="-10" width="0.05" layer="39"/>
<wire x1="-6.5" y1="-10" x2="-9.25" y2="-10" width="0.05" layer="39"/>
<wire x1="-9.25" y1="-10" x2="-9.25" y2="-9" width="0.05" layer="39"/>
<wire x1="-9.25" y1="-9" x2="-10.1" y2="-9" width="0.05" layer="39"/>
<wire x1="-10.1" y1="-9" x2="-10.1" y2="9" width="0.05" layer="39"/>
<wire x1="-10.1" y1="9" x2="-9.25" y2="9" width="0.05" layer="39"/>
<wire x1="-9.25" y1="9" x2="-9.25" y2="16" width="0.05" layer="39"/>
<text x="-9.01326875" y="16.2739" size="1.67886875" layer="25">&gt;NAME</text>
<text x="-9.02085" y="-12.529" size="1.680290625" layer="27">&gt;VALUE</text>
<rectangle x1="-9.007809375" y1="9.007809375" x2="9" y2="15.75" layer="41"/>
<rectangle x1="-9.01791875" y1="9.01791875" x2="9" y2="15.75" layer="42"/>
<rectangle x1="-9.01766875" y1="9.01766875" x2="9" y2="15.75" layer="43"/>
<circle x="-10.365" y="8.2754" radius="0.1" width="0.2" layer="21"/>
<smd name="1" x="-9" y="8.255" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="2" x="-9" y="6.985" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="3" x="-9" y="5.715" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="4" x="-9" y="4.445" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="5" x="-9" y="3.175" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="6" x="-9" y="1.905" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="7" x="-9" y="0.635" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="8" x="-9" y="-0.635" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="9" x="-9" y="-1.905" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="10" x="-9" y="-3.175" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="11" x="-9" y="-4.445" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="12" x="-9" y="-5.715" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="13" x="-9" y="-6.985" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="14" x="-9" y="-8.255" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="15" x="-5.715" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="16" x="-4.445" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="17" x="-3.175" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="18" x="-1.905" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="19" x="-0.635" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="20" x="0.635" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="21" x="1.905" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="22" x="3.175" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="23" x="4.445" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="24" x="5.715" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="25" x="9" y="-8.255" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="26" x="9" y="-6.985" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="27" x="9" y="-5.715" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="28" x="9" y="-4.445" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="29" x="9" y="-3.175" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="30" x="9" y="-1.905" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="31" x="9" y="-0.635" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="32" x="9" y="0.635" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="33" x="9" y="1.905" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="34" x="9" y="3.175" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="35" x="9" y="4.445" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="36" x="9" y="5.715" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="37" x="9" y="6.985" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="38" x="9" y="8.255" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="1.1" x="0.7" y="0.845" dx="4" dy="4" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="ESP-WROOM-32">
<wire x1="-15.24" y1="-27.94" x2="15.24" y2="-27.94" width="0.254" layer="94"/>
<wire x1="15.24" y1="-27.94" x2="15.24" y2="27.94" width="0.254" layer="94"/>
<wire x1="15.24" y1="27.94" x2="-15.24" y2="27.94" width="0.254" layer="94"/>
<wire x1="-15.24" y1="27.94" x2="-15.24" y2="-27.94" width="0.254" layer="94"/>
<text x="-15.2559" y="28.4776" size="2.542640625" layer="95">&gt;NAME</text>
<text x="-15.2552" y="-31.0191" size="2.54255" layer="96">&gt;VALUE</text>
<pin name="GND" x="20.32" y="-25.4" length="middle" direction="pwr" rot="R180"/>
<pin name="3V3" x="20.32" y="25.4" length="middle" direction="pwr" rot="R180"/>
<pin name="EN" x="-20.32" y="20.32" length="middle" direction="in"/>
<pin name="SENSOR_VP" x="-20.32" y="17.78" length="middle" direction="in"/>
<pin name="SENSOR_VN" x="-20.32" y="15.24" length="middle" direction="in"/>
<pin name="IO34" x="20.32" y="-17.78" length="middle" rot="R180"/>
<pin name="IO35" x="20.32" y="-20.32" length="middle" rot="R180"/>
<pin name="TXD0" x="-20.32" y="12.7" length="middle"/>
<pin name="RXD0" x="-20.32" y="10.16" length="middle"/>
<pin name="SWP/SD3" x="-20.32" y="-5.08" length="middle"/>
<pin name="SHD/SD2" x="-20.32" y="-2.54" length="middle"/>
<pin name="SCS/CMD" x="-20.32" y="7.62" length="middle"/>
<pin name="SCK/CLK" x="-20.32" y="5.08" length="middle" function="clk"/>
<pin name="SDO/SD0" x="-20.32" y="2.54" length="middle"/>
<pin name="SDI/SD1" x="-20.32" y="0" length="middle"/>
<pin name="IO0" x="-20.32" y="-10.16" length="middle"/>
<pin name="IO2" x="-20.32" y="-12.7" length="middle"/>
<pin name="IO4" x="-20.32" y="-15.24" length="middle"/>
<pin name="IO5" x="-20.32" y="-17.78" length="middle"/>
<pin name="IO12" x="-20.32" y="-20.32" length="middle"/>
<pin name="IO13" x="20.32" y="20.32" length="middle" rot="R180"/>
<pin name="IO14" x="20.32" y="17.78" length="middle" rot="R180"/>
<pin name="IO15" x="20.32" y="15.24" length="middle" rot="R180"/>
<pin name="IO16" x="20.32" y="12.7" length="middle" rot="R180"/>
<pin name="IO17" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="IO18" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="IO19" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="IO21" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="IO22" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="IO23" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="IO25" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="IO26" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="IO27" x="20.32" y="-10.16" length="middle" rot="R180"/>
<pin name="IO32" x="20.32" y="-12.7" length="middle" rot="R180"/>
<pin name="IO33" x="20.32" y="-15.24" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ESP-WROOM-32" prefix="U">
<description>Module: combo; GPIO, I2C x2, I2S x2, SDIO, SPI x3, UART x3; 19.5dBm</description>
<gates>
<gate name="G$1" symbol="ESP-WROOM-32" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MODULE_ESP-WROOM-32">
<connects>
<connect gate="G$1" pin="3V3" pad="2"/>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="1 1.1 15 38"/>
<connect gate="G$1" pin="IO0" pad="25"/>
<connect gate="G$1" pin="IO12" pad="14"/>
<connect gate="G$1" pin="IO13" pad="16"/>
<connect gate="G$1" pin="IO14" pad="13"/>
<connect gate="G$1" pin="IO15" pad="23"/>
<connect gate="G$1" pin="IO16" pad="27"/>
<connect gate="G$1" pin="IO17" pad="28"/>
<connect gate="G$1" pin="IO18" pad="30"/>
<connect gate="G$1" pin="IO19" pad="31"/>
<connect gate="G$1" pin="IO2" pad="24"/>
<connect gate="G$1" pin="IO21" pad="33"/>
<connect gate="G$1" pin="IO22" pad="36"/>
<connect gate="G$1" pin="IO23" pad="37"/>
<connect gate="G$1" pin="IO25" pad="10"/>
<connect gate="G$1" pin="IO26" pad="11"/>
<connect gate="G$1" pin="IO27" pad="12"/>
<connect gate="G$1" pin="IO32" pad="8"/>
<connect gate="G$1" pin="IO33" pad="9"/>
<connect gate="G$1" pin="IO34" pad="6"/>
<connect gate="G$1" pin="IO35" pad="7"/>
<connect gate="G$1" pin="IO4" pad="26"/>
<connect gate="G$1" pin="IO5" pad="29"/>
<connect gate="G$1" pin="RXD0" pad="34"/>
<connect gate="G$1" pin="SCK/CLK" pad="20"/>
<connect gate="G$1" pin="SCS/CMD" pad="19"/>
<connect gate="G$1" pin="SDI/SD1" pad="22"/>
<connect gate="G$1" pin="SDO/SD0" pad="21"/>
<connect gate="G$1" pin="SENSOR_VN" pad="5"/>
<connect gate="G$1" pin="SENSOR_VP" pad="4"/>
<connect gate="G$1" pin="SHD/SD2" pad="17"/>
<connect gate="G$1" pin="SWP/SD3" pad="18"/>
<connect gate="G$1" pin="TXD0" pad="35"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Good"/>
<attribute name="DESCRIPTION" value=" Module: combo; GPIO, I2C x2, I2S x2, SDIO, SPI x3, UART x3; 19.5dBm "/>
<attribute name="MF" value="Espressif Systems"/>
<attribute name="MP" value="ESP-WROOM-32"/>
<attribute name="PACKAGE" value="Module Espressif Systems"/>
<attribute name="PRICE" value="5.02 USD"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="ESP-WROOM-32" deviceset="ESP-WROOM-32" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U1" gate="G$1" x="40.64" y="66.04" smashed="yes">
<attribute name="NAME" x="25.3841" y="94.5176" size="2.542640625" layer="95"/>
<attribute name="VALUE" x="25.3848" y="35.0209" size="2.54255" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
